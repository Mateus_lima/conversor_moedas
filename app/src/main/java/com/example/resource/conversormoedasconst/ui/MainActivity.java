package com.example.resource.conversormoedasconst.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.resource.conversormoedasconst.R;
import com.example.resource.conversormoedasconst.api.DataGetter;
import com.example.resource.conversormoedasconst.domain.Conversor;
import com.example.resource.conversormoedasconst.domain.Moeda;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    public EditText editValue;
    public TextView textDolar;
    public TextView textEuro;
    public Button buttonCalcular;
    public DataGetter dtgDolar = new DataGetter();
    public DataGetter dtgEuro = new DataGetter();
    public boolean conectado;
    public AlertDialog alerta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        editValue = findViewById(R.id.edit_value);
        textDolar = findViewById(R.id.text_dolar);
        textEuro = findViewById(R.id.text_euro);
        buttonCalcular = findViewById(R.id.button_calcular);

        if (verificaConexao()) {
            String url = "https://economia.awesomeapi.com.br/";
            dtgDolar.execute(url + "USD-BRL");
            dtgDolar.c = MainActivity.this;
            dtgEuro.execute(url + "eur");
            dtgEuro.c = MainActivity.this;
        } else {
            alerta();
        }

        //verificar se a quantidade de caracteres no campo é igual a zero
        editValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    clearValues();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        this.clearValues();//inicia os campos  text limpos
        buttonCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonConversorOnClick();
            }
        });
    }

    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    public void buttonConversorOnClick() {
        try {
            if (verificaConexao()) {
                Conversor dolar = new Conversor(Double.valueOf(editValue.getText().toString())
                        , new Moeda(dtgDolar.getValor(), dtgDolar.getMoeda()));

                Conversor euro = new Conversor(Double.valueOf(editValue.getText().toString()),
                        new Moeda(dtgEuro.getValor(), dtgEuro.getMoeda()));

                textDolar.setText("$ " + String.format("%.2f", dolar.Converter()) + " " +
                        dolar.getMoeda().getNome());

                textEuro.setText("€ " + String.format("%.2f", euro.Converter()) + " " +
                        euro.getMoeda().getNome());
            } else {
                Conversor dolar = new Conversor(Double.valueOf(editValue.getText().toString()),
                        new Moeda(3, "dolar fixo"));

                Conversor euro = new Conversor(Double.valueOf(editValue.getText().toString()),
                        new Moeda(4, "euro fixo"));

                textDolar.setText("$ " + String.format("%.2f", dolar.Converter()) + " " +
                        dolar.getMoeda().getNome());

                textEuro.setText("€ " + String.format("%.2f", euro.Converter()) + " " +
                        euro.getMoeda().getNome());
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void clearValues() {
        textEuro.setText("0");
        textDolar.setText("0");
    }

    public boolean verificaConexao() {

        ConnectivityManager conectivtyManager = (ConnectivityManager) getSystemService(Context
                .CONNECTIVITY_SERVICE);
        conectado = conectivtyManager.getActiveNetworkInfo() != null && conectivtyManager.getActiveNetworkInfo()
                .isAvailable() && conectivtyManager.getActiveNetworkInfo().isConnected();
        
        return conectado;
    }



    private void alerta() {
        //Cria o gerador do AlertDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //define o titulo
        builder.setTitle("Sem Conexão");

        //define a mensagem
        builder.setMessage("Seu dispositivo não está conectado a internet, ligue o wi-fi " +
                "ou dados moveis para ter uma conversão precisa");

        //define um botão como positivo
        builder.setPositiveButton("Ok, vou ligar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

                WifiManager wifi = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                wifi.setWifiEnabled(true); // Liga o WiFi

                    wifi.setWifiEnabled(true);

                Intent i = new Intent(MainActivity.this,MainActivity.class);
                startActivity(i);
                finish();

                //finish();
            }
        });

        //define um botão como negativo
        builder.setNegativeButton("Ok, continuar mesmo assim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });

        //cria o AlertDialog
        alerta = builder.create();
        //Exibe
        alerta.show();
    }



}
